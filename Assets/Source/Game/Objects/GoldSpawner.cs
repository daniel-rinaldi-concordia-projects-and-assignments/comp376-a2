﻿using UnityEngine;

namespace Game.Objects
{
    public class GoldSpawner : MonoBehaviour
    {
        [SerializeField] private Gold goldPrefab;
        [SerializeField] private int maxGoldToSpawn = 8;
        [SerializeField] private float goldSpawnTime = 3;

        private RectTransform gold_area;
        private int gold_spawned;
        private float gold_timer;

        private void Awake()
        {
            gold_area = this.transform.GetChild(0).GetComponent<RectTransform>();
            gold_spawned = 0;
            gold_timer = goldSpawnTime;
        }

        private void Update()
        {
            if (gold_spawned < maxGoldToSpawn)
            {
                if (UpdateTimer(ref gold_timer, goldSpawnTime))
                {
                    Spawn();
                }
            }

            gold_spawned = gold_area.childCount;
        }

        private void Spawn()
        {
            Gold gold = Instantiate(goldPrefab, gold_area);
            gold.Type = (Gold.GoldType)Random.Range((int)Gold.GoldType.COINS, ((int)Gold.GoldType.CHEST) + 1);

            float[] range = { gold_area.position.x + 0.16f, gold_area.position.x + (gold_area.rect.width - 0.16f) };
            gold.transform.position = new Vector3(Random.Range(range[0], range[1]), gold_area.position.y, gold.transform.position.z);
        }

        private bool UpdateTimer(ref float timer, float resetTime)
        {
            if (timer > 0f)
                timer -= Time.deltaTime;
            else
            {
                timer = resetTime;
                return true;
            }

            return false;
        }
    }
}