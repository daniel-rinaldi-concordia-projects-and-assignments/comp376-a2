﻿using UnityEngine;

using Game.Audio;

namespace Game.Objects
{
    public class Octopuss : MonoBehaviour
    {
        [SerializeField] private GameObject player;

        [SerializeField] [Min(15f)] private float maxSpawnTime = 15f;
        private float spawn_timer = 0f;

        [SerializeField] [Min(15f)] private float maxDespawnTime = 15f;
        private float despawn_timer = 0f;

        [SerializeField] private RectTransform armNotificationArea;
        [SerializeField] private GameObject leftArm;
        [SerializeField] private GameObject rightArm;

        [SerializeField] private float moveSpeed = 1.5f;

        private Animator animator;
        private Vector2 starting_position;
        private bool chasing_player;
        private bool is_finished_despawning;
        private float max_time_between_attacks;
        private float left_arm_timer = 0f;
        private float right_arm_timer = 0f;
        private bool attack_left;
        private bool retract_left;
        private bool attack_right;
        private bool retract_right;

        private enum Mode { NONE, SPAWNED }
        private Mode mode;

        private void Awake()
        {
            mode = Mode.NONE;
            spawn_timer = Random.Range(15f, maxSpawnTime);
            despawn_timer = Random.Range(15f, maxDespawnTime);
            starting_position = this.transform.position;
            is_finished_despawning = true;
            max_time_between_attacks = 8f;
            left_arm_timer = Random.Range(3f, max_time_between_attacks);
            right_arm_timer = Random.Range(3f, max_time_between_attacks);
            animator = this.GetComponent<Animator>();
        }

        private void Update()
        {
            if (mode == Mode.NONE)
            {
                if (this.transform.position.y != starting_position.y)
                    this.transform.position = Vector2.MoveTowards(this.transform.position, new Vector2(this.transform.position.x, starting_position.y), moveSpeed * Time.deltaTime);
                else if (this.transform.position.x != starting_position.x)
                    this.transform.position = starting_position;
                else if (UpdateTimer(ref spawn_timer, maxSpawnTime))
                {
                    Spawn();
                    spawn_timer = Random.Range(15f, maxSpawnTime);
                }

                if (!is_finished_despawning && (Vector2)this.transform.position == starting_position)
                {
                    is_finished_despawning = true;
                    animator.enabled = false;
                    retract_left = retract_right = true;
                    attack_left = attack_right = false;
                    leftArm.transform.position = rightArm.transform.position = new Vector3(starting_position.x, this.transform.position.y - 3.5f, this.transform.position.z);
                }
            }
            else if (mode == Mode.SPAWNED)
            {
                if (this.transform.position.y != -6.4f)
                    this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(starting_position.x, -6.4f, this.transform.position.z), moveSpeed * Time.deltaTime);
                else if (UpdateTimer(ref despawn_timer, maxDespawnTime))
                {
                    Despawn();
                    despawn_timer = Random.Range(15f, maxDespawnTime);
                }
                else if (!attack_left && UpdateTimer(ref left_arm_timer, max_time_between_attacks))
                {
                    attack_left = true;
                    retract_left = false;
                    leftArm.transform.position = new Vector3(player != null ? player.transform.position.x : this.transform.position.x, leftArm.transform.position.y, leftArm.transform.position.z);
                    left_arm_timer = Random.Range(3f, max_time_between_attacks);
                }
                else if (!attack_right && UpdateTimer(ref right_arm_timer, max_time_between_attacks))
                {
                    attack_right = true;
                    retract_right = false;
                    rightArm.transform.position = new Vector3(player != null ? player.transform.position.x : this.transform.position.x, rightArm.transform.position.y, rightArm.transform.position.z);
                    right_arm_timer = Random.Range(3f, max_time_between_attacks);
                }

                if (chasing_player && player != null && this.transform.position.y == -6.4f)
                {
                    Vector3 left_temp = leftArm.transform.position;
                    Vector3 right_temp = rightArm.transform.position;
                    this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(player.transform.position.x, this.transform.position.y, this.transform.position.z), moveSpeed * 0.4f * Time.deltaTime);
                    leftArm.transform.position = left_temp;
                    rightArm.transform.position = right_temp;
                }

                UpdateArms();
            }
        }

        private void UpdateArms()
        {
            if (attack_left)
            {
                if (!retract_left)
                {
                    if (leftArm.transform.position.y < armNotificationArea.transform.position.y - 1)
                    {
                        GameObject notification_obj = armNotificationArea.GetChild(0).gameObject;
                        notification_obj.transform.position = new Vector3(leftArm.transform.position.x, notification_obj.transform.position.y, notification_obj.transform.position.z);
                        notification_obj.SetActive(true);
                    }

                    leftArm.transform.position = Vector3.MoveTowards(leftArm.transform.position, new Vector3(leftArm.transform.position.x, this.transform.position.y, leftArm.transform.position.z), moveSpeed * 0.8f * Time.deltaTime);
                    if (Mathf.Approximately(leftArm.transform.position.y, this.transform.position.y))
                        retract_left = true;
                }
                else
                {
                    armNotificationArea.GetChild(0).gameObject.SetActive(false);

                    leftArm.transform.position = Vector3.MoveTowards(leftArm.transform.position, new Vector3(leftArm.transform.position.x, this.transform.position.y - 3.5f, leftArm.transform.position.z), moveSpeed * 0.8f * Time.deltaTime);
                    if (Mathf.Approximately(leftArm.transform.position.y, this.transform.position.y - 3.5f))
                        attack_left = false;
                }
            }
            if (attack_right)
            {
                if (!retract_right)
                {
                    if (rightArm.transform.position.y < armNotificationArea.transform.position.y - 1)
                    {
                        GameObject notification_obj = armNotificationArea.GetChild(1).gameObject;
                        notification_obj.transform.position = new Vector3(rightArm.transform.position.x, notification_obj.transform.position.y, notification_obj.transform.position.z);
                        notification_obj.SetActive(true);
                    }

                    rightArm.transform.position = Vector3.MoveTowards(rightArm.transform.position, new Vector3(rightArm.transform.position.x, this.transform.position.y, rightArm.transform.position.z), moveSpeed * 0.8f * Time.deltaTime);
                    if (Mathf.Approximately(rightArm.transform.position.y, this.transform.position.y))
                        retract_right = true;
                }
                else
                {
                    armNotificationArea.GetChild(1).gameObject.SetActive(false);
                    rightArm.transform.position = Vector3.MoveTowards(rightArm.transform.position, new Vector3(rightArm.transform.position.x, this.transform.position.y - 3.5f, rightArm.transform.position.z), moveSpeed * 0.8f * Time.deltaTime);
                    if (Mathf.Approximately(rightArm.transform.position.y, this.transform.position.y - 3.5f))
                        attack_right = false;
                }
            }
        }

        private void Spawn()
        {
            animator.enabled = true;
            chasing_player = true;

            mode = Mode.SPAWNED;
            AudioManager.Instance.PlayMusic("The Evil King Koopa", new AudioManager.MusicOptions(AudioManager.MusicTransition.FADE));
        }

        private void Despawn()
        {
            left_arm_timer = Random.Range(3f, max_time_between_attacks);
            right_arm_timer = Random.Range(3f, max_time_between_attacks);
            chasing_player = false;
            is_finished_despawning = false;
            armNotificationArea.GetChild(0).gameObject.SetActive(false);
            armNotificationArea.GetChild(1).gameObject.SetActive(false);
            retract_left = retract_right = true;

            mode = Mode.NONE;
            AudioManager.Instance.PlayMusic("Silence", new AudioManager.MusicOptions(AudioManager.MusicTransition.FADE));
        }

        private bool UpdateTimer(ref float timer, float resetTime)
        {
            if (timer > 0f)
                timer -= Time.deltaTime;
            else
            {
                timer = resetTime;
                return true;
            }

            return false;
        }
    }
}