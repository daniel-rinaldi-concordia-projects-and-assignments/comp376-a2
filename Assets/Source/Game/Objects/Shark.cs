﻿using UnityEngine;

namespace Game.Objects
{
    public class Shark : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer sprite;
        [Min(0.1f)] [SerializeField] private float maxSharkScale;
        [Min(0.1f)] [SerializeField] private float minSharkScale;

        [Min(0.1f)] [SerializeField] private float baseSpeed = 0.7f;
        public float BaseSpeed
        {
            get { return baseSpeed; }
            set
            {
                baseSpeed = value;
                Speed = baseSpeed / (1 + (this.transform.localScale.x - 1));
            }
        }

        public float Speed { get; set; }
        public bool Flipped { get; set; }

        private AudioSource sound_effect;

        private void Awake()
        {
            sound_effect = this.GetComponent<AudioSource>();

            BaseSpeed = baseSpeed;
            Speed = baseSpeed;
        }

        public void Flip()
        {
            Flipped = Flipped ? false : true;
            this.transform.Rotate(0, 180f, 0);
        }

        public void Resize()
        {
            int rand = Random.Range((int)(minSharkScale * 10), (int)(maxSharkScale * 10));
            float shark_scale = rand / 10f;
            Resize(shark_scale);
        }
        public void Resize(float scale)
        {
            this.transform.localScale = new Vector3(scale, scale, this.transform.localScale.z);
            Speed = baseSpeed / (1 + (scale - 1));
        }

        public void PlaySoundEffect()
        {
            sound_effect.Play();
        }
    }
}