﻿using UnityEngine;

using System.Collections.Generic;

namespace Game.Objects
{
    public class SharkSpawner : MonoBehaviour
    {
        [SerializeField] private RectTransform environment;

        [Tooltip("The default height of the shark's sprite (in world units)")]
        [SerializeField] private float sharkSpriteDefaultHeight = 0.32f;

        private List<GameObject> shark_lanes;
        private int shark_lanes_in_use;
        private int current_level;

        private void Awake()
        {
            shark_lanes = new List<GameObject>();
            shark_lanes_in_use = 3;
            GameObject shark_lane_objects = this.transform.GetChild(0).gameObject;
            for (int i = 0; i < shark_lane_objects.transform.childCount; i++)
                shark_lanes.Add(shark_lane_objects.transform.GetChild(i).gameObject);
        }

        private void Start()
        {
            ObjectPooler.Instance.ResizePool("Shark", 15);
            for (int i = 0; i < shark_lanes_in_use; i++)
                SpawnShark(i);
        }

        private void Update()
        {
            for (int i = 0; i < shark_lanes_in_use; i++)
            {
                GameObject shark_obj = shark_lanes[i].transform.GetChild(0).gameObject;
                Collider2D shark_collider = shark_obj.GetComponent<Collider2D>();
                Shark shark = shark_obj.GetComponent<Shark>();
                bool moving_right = !shark.Flipped;
                float destination_end = (environment.rect.width / 2) + (shark_collider.bounds.size.x * 2);

                if (moving_right && shark_obj.transform.position.x >= destination_end || !moving_right && shark_obj.transform.position.x <= -destination_end)
                {
                    shark.Flip();
                    shark.Resize();
                    float shark_width = shark_collider.bounds.size.x;
                    shark_obj.transform.position = new Vector3(shark_obj.transform.position.x + (shark.Flipped ? shark_width : -shark_width), shark_obj.transform.position.y, shark_obj.transform.position.z);
                }

                shark_obj.transform.position = Vector2.MoveTowards(
                    shark_obj.transform.position,
                    new Vector2(this.transform.position.x + (moving_right ? destination_end : -destination_end), shark_lanes[i].transform.position.y),
                    shark.Speed * Time.deltaTime
                );

                if (current_level != GameManager.Instance.CurrentLevel)
                    shark.BaseSpeed += 0.05f;
            }

            if (current_level != GameManager.Instance.CurrentLevel)
            {
                if (shark_lanes_in_use < shark_lanes.Count)
                {
                    shark_lanes_in_use++;
                    SpawnShark(shark_lanes_in_use - 1);
                }

                current_level = GameManager.Instance.CurrentLevel;
            }
        }

        private GameObject SpawnShark(int lane_index)
        {
            float environment_half_width = environment.rect.width / 2;

            GameObject shark_obj = ObjectPooler.Instance.Spawn(
                "Shark",
                new Vector3((lane_index % 2 == 0 ? -environment_half_width : environment_half_width), 0, shark_lanes[lane_index].transform.position.z),
                Quaternion.identity,
                shark_lanes[lane_index]
            );

            Shark shark = shark_obj.GetComponent<Shark>();

            if (lane_index % 2 != 0)
                shark.Flip();

            shark.Resize();
            float shark_width = shark_obj.GetComponent<Collider2D>().bounds.size.x;
            shark_obj.transform.position = new Vector3(shark_obj.transform.position.x + (shark.Flipped ? shark_width : -shark_width), shark_obj.transform.position.y, shark_obj.transform.position.z);

            return shark_obj;
        }
    }
}