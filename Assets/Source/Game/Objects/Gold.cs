﻿using UnityEngine;

namespace Game.Objects
{
    public class Gold : MonoBehaviour
    {
        public enum GoldType { NONE, COINS, BARS, CHEST }

        [SerializeField] private GoldType goldType;

        private GoldType current_type;
        private GameObject sprites_object;

        private void Awake()
        {
            sprites_object = this.transform.GetChild(0).gameObject;
        }

        private void Update()
        {
            if (current_type != goldType)
            {
                sprites_object.transform.GetChild((int)current_type).gameObject.SetActive(false);
                sprites_object.transform.GetChild((int)goldType).gameObject.SetActive(true);
                current_type = goldType;
            }
        }

        public GoldType Type
        {
            get { return goldType; }
            set { goldType = value; }
        }
    }
}