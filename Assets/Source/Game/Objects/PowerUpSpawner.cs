﻿using UnityEngine;

using Game.Player;

namespace Game.Objects
{
    public class PowerUpSpawner : MonoBehaviour
    {
        [SerializeField] private PlayerMovement player;
        [SerializeField] private PowerUp powerUpPrefab;
        [SerializeField] private float spawnTime = 60f;
        [SerializeField] private int maxPowerUpsToSpawn = 1;
        private float spawn_timer;

        private RectTransform powerup_area;
        private int powerups_spawned;

        private void Awake()
        {
            powerup_area = this.transform.GetChild(0).GetComponent<RectTransform>();
            spawn_timer = spawnTime;
            powerups_spawned = 0;
        }

        private void Update()
        {
            if (powerups_spawned < maxPowerUpsToSpawn)
            {
                if (UpdateTimer(ref spawn_timer, spawnTime))
                {
                    Spawn();
                }
            }

            powerups_spawned = powerup_area.childCount + player.powerups.Count;
        }

        private void Spawn()
        {
            PowerUp powerup = Instantiate(powerUpPrefab, powerup_area);

            float[] range = { powerup_area.position.x + 0.16f, powerup_area.position.x + (powerup_area.rect.width - 0.16f) };
            powerup.transform.position = new Vector3(Random.Range(range[0], range[1]), powerup_area.position.y, powerup.transform.position.z);
        }

        private bool UpdateTimer(ref float timer, float resetTime)
        {
            if (timer > 0f)
                timer -= Time.deltaTime;
            else
            {
                timer = resetTime;
                return true;
            }

            return false;
        }
    }
}