﻿using UnityEngine;
using UnityEngine.UI;

using Game.Audio;

namespace Game.Scenes
{
    public class GameplayScene : MonoBehaviour
    {
        [SerializeField] private float levelChangeTime;
        private float level_change_timer;

        [SerializeField] private Text levelText;
        [SerializeField] private GameObject powerUpSpawnerObject;

        private void Awake()
        {
            AudioManager.Instance.PlayMusic("Silence", new AudioManager.MusicOptions(AudioManager.MusicTransition.FADE));

            level_change_timer = levelChangeTime;
            GameManager.Instance.CurrentLevel = 1;
            levelText.text = "LEVEL " + GameManager.Instance.CurrentLevel.ToString();
            GameManager.Instance.Score = 0;

            if (powerUpSpawnerObject != null)
            {
                if (GameManager.Instance.GameMode == GameManager.Mode.SPECIAL)
                    powerUpSpawnerObject.SetActive(true);
                else
                    powerUpSpawnerObject.SetActive(false);
            }
                
        }

        private void Update()
        {
            if (UpdateTimer(ref level_change_timer, levelChangeTime))
            {
                GameManager.Instance.CurrentLevel++;
                levelText.text = "LEVEL " + GameManager.Instance.CurrentLevel.ToString();
            }
        }

        private bool UpdateTimer(ref float timer, float resetTime)
        {
            if (timer > 0f)
                timer -= Time.deltaTime;
            else
            {
                timer = resetTime;
                return true;
            }

            return false;
        }
    }
}