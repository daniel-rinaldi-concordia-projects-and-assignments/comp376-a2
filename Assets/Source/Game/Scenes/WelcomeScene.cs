﻿using UnityEngine;

using Game.UI;
using Game.UI.Menus;
using Game.Audio;

namespace Game.Scenes
{
    public class WelcomeScene : MonoBehaviour
    {
        private void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            UIManager.Instance.OpenUI<MainMenu>();
            AudioManager.Instance.PlayMusic("Silence", new AudioManager.MusicOptions(AudioManager.MusicTransition.FADE));
        }
    }
}