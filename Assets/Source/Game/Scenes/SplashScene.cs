﻿using UnityEngine;

using Game.Audio;
using Game.Scenes;

public class SplashScene : MonoBehaviour
{
    [SerializeField] private string firstScene = "";

    private void Awake()
    {
        AudioManager.Instance.PlayMusic("Silence");
    }

    public void OnSplashComplete()
    {
        SceneLoader.Instance.LoadScene(firstScene, SceneLoader.TransitionType.FADE);
    }
}
