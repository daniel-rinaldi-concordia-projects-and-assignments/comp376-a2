﻿using UnityEngine;
using UnityEngine.Audio;

using System.Collections;
using System.Collections.Generic;

namespace Game.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioManager : MBSingleton<AudioManager>
    {
        [SerializeField] private AudioMixer masterMixer;
        [SerializeField] private AudioMixerGroup masterGroup;
        [SerializeField] private AudioMixerGroup musicGroup;
        [SerializeField] private AudioMixerGroup soundGroup;

        private Dictionary<string, int> music_dictionary;
        [Tooltip("List of music audio clips")]
        [SerializeField] private List<AudioClip> musicList;

        public enum MusicTransition { NONE, FADE }

        private AudioSource source;
        public class MusicOptions
        {
            public bool loop;
            public MusicTransition music_transition;
            public MusicOptions(MusicTransition music_transition = MusicTransition.NONE, bool loop = true)
            {
                this.loop = loop;
                this.music_transition = music_transition;
            }
        }

        protected override void Awake()
        {
            base.Awake();

            music_dictionary = new Dictionary<string, int>();

            if (musicList == null) musicList = new List<AudioClip>();

            // link dictionary entries (because unity does not support dictionaries as Serialized fields in the editor yet, lame...)
            for (int i = 0; i < musicList.Count; i++)
                music_dictionary.Add(musicList[i].name, i);

            source = this.GetComponent<AudioSource>();
            source.priority = 0;
        }

        public AudioClip GetMusic(string name)
        {
            if (music_dictionary.ContainsKey(name))
                return musicList[music_dictionary[name]];
            else
            {
                Debug.LogError("[AudioManager] : Music with name '" + name + "' not found in music list.");
                return null;
            }
        }

        public void PlayMusic(string name, MusicOptions options = null)
        {
            Play(GetMusic(name), (options != null ? options : new MusicOptions()), musicGroup);
        }
        public void Play(AudioClip clip, MusicOptions options = null, AudioMixerGroup output = null)
        {
            if (options == null) options = new MusicOptions();

            if (source.clip != null && source.clip.name == clip.name)
            {
                SetMusicOptions(options);
                return;
            }

            if (output != null) source.outputAudioMixerGroup = output;

            SetMusicOptions(options);

            float volume = source.volume;

            if (options.music_transition == MusicTransition.FADE)
                StartCoroutine(MusicFadeTransition(clip, volume));
            else
            {
                source.Stop();
                source.clip = clip;
                source.Play();
            }
        }

        public void Stop()
        {
            source.Stop();
        }

        public void SetMusicOptions(MusicOptions options)
        {
            source.loop = options.loop;
        }

        public void SetMasterVolume(float volume)
        {
            masterMixer.SetFloat("MasterVolume", volume);
        }

        public void SetMusicVolume(float volume)
        {
            masterMixer.SetFloat("MusicVolume", volume);
        }

        public void SetSoundVolume(float volume)
        {
            masterMixer.SetFloat("SoundVolume", volume);
        }

        private IEnumerator MusicFadeTransition(AudioClip clip, float max_volume)
        {
            while (source.volume > 0f)
            {
                source.volume -= 0.1f;
                yield return new WaitForSeconds(0.05f);
            }

            source.Stop();
            source.clip = clip;
            source.Play();

            while (source.volume < max_volume)
            {
                source.volume += 0.1f;
                yield return new WaitForSeconds(0.05f);
            }
        }
    }
}