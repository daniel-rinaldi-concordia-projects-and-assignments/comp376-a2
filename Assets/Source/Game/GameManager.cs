﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameManager : MBSingleton<GameManager>
    {
        public enum Mode { NORMAL, SPECIAL }
        public Mode GameMode { get; set; }

        public int CurrentLevel { get; set; }
        public int Score { get; set; }

        protected override void Awake()
        {
            base.Awake();
            GameMode = Mode.NORMAL;
            CurrentLevel = -1;
            Score = 0;
        }

        private void Start()
        {
            // load first Scene
            if (Application.isEditor) // <-- we are not in production
                SceneManager.LoadScene("Level_1");
            else
                SceneManager.LoadScene("Splash");
        }

        protected override void OnApplicationQuit()
        {
            base.OnApplicationQuit();
        }
    }
}
