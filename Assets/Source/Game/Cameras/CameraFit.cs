﻿using UnityEngine;

namespace Game.Cameras
{
    [RequireComponent(typeof(Camera))]
    public class CameraFit : MonoBehaviour
    {
        [SerializeField] private float screenWidth = 0;

        private Camera cam;
        private Vector2 screen_dimensions;

        private void Awake()
        {
            cam = this.GetComponent<Camera>();

            FixCameraSizeToScreenWidth();
        }

        private void FixedUpdate()
        {
            if (screen_dimensions.x != Screen.width && screen_dimensions.y != Screen.height)
                FixCameraSizeToScreenWidth();
        }

        private void FixCameraSizeToScreenWidth()
        {
            if (screenWidth != 0f)
            {
                cam.orthographicSize = screenWidth * ((float)Screen.height / (float)Screen.width) * 0.5f;
                screen_dimensions = new Vector2(Screen.width, Screen.height);
            }
        }
    }
}
