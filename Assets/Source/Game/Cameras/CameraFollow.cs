﻿using UnityEngine;

namespace Game.Cameras
{
    [RequireComponent(typeof(Camera))]
    public class CameraFollow : MonoBehaviour
    {
        [Tooltip("The target this camera is interested in")]
        [SerializeField] private Transform target;

        [Range(0, 1)]
        [Tooltip("How fast the camera will snap to the target")]
        [SerializeField] private float smoothing = 0.125f;

        private Camera cam;

        private void Awake()
        {
            cam = this.GetComponent<Camera>();
        }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Update()
        {
            if (target != null)
            {
                Vector2 desired_position = CheckBounds(target.position);
                Vector3 pos = this.transform.position;

                Vector2 smoothed_position = Vector2.Lerp(pos, desired_position, smoothing);
                this.transform.position = new Vector3(smoothed_position.x, smoothed_position.y, pos.z);
            }
        }

        private Vector2 CheckBounds(Vector2 position)
        {
            float x = this.transform.position.x;
            float y = position.y;

            float top = 0 - cam.orthographicSize;
            float bottom = -7.68f + cam.orthographicSize;
            y = Mathf.Clamp(y, bottom, top);
            
            return new Vector2(x, y);
        }
    }
}
