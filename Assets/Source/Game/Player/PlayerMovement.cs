﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections.Generic;

using Game.Objects;
using Game.Scenes;

namespace Game.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMovement : MonoBehaviour
    {
        [Tooltip("How fast the player can move")]
        [SerializeField] private float moveSpeed = 40f;

        [Tooltip("The player's sprite renderer")]
        [SerializeField] private SpriteRenderer spriteRenderer;

        [Tooltip("The force at which the player moves up")]
        [SerializeField] private float upwardsForce = 60f;

        [SerializeField] private float sinkVelocity = -30f;

        [SerializeField] private SpriteRenderer bubbles_sprite;

        [SerializeField] private GameObject healthUIObject;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text goldText;
        [SerializeField] private GameObject powerUpUIObject;
        [SerializeField] private Text lifeCounterText;

        [SerializeField] private Collider2D vertical_collider;
        [SerializeField] private Collider2D horizontal_collider;
        private Collider2D player_collider;

        [SerializeField] private float damageInvincibilityTime = 2f;
        private float damage_invincibility_timer;
        private bool damage_invincibility;

        [SerializeField] private AudioSource goldPickupSFX;

        private Rigidbody2D rigidbody_2d;
        private bool facing_right;
        private float upwards_timer;
        private float upwards_delay;
        private float speed_modifier;
        private int gold_on_hand;
        private float weight;
        private int lives;
        private int health;
        [HideInInspector] public List<PowerUp> powerups;
        private int max_powerups_on_hand;
        private float speed_boost_time;
        private float speed_boost_timer;
        private Vector2 starting_position;

        private Animator animator;
        private bool moving;
        private bool moving_horizontally;
        private bool hurting;
        private bool dying;

        private void Awake()
        {
            animator = this.GetComponent<Animator>();
            rigidbody_2d = this.GetComponent<Rigidbody2D>();

            horizontal_collider.enabled = false;
            vertical_collider.enabled = true;
            player_collider = vertical_collider;
            upwards_delay = 0.9f;
            upwards_timer = 0;
            speed_modifier = 1f;
            gold_on_hand = 0;
            weight = 1;
            lives = 2;
            lifeCounterText.text = lives.ToString();
            health = 2;
            damage_invincibility_timer = damageInvincibilityTime;
            powerups = new List<PowerUp>();
            max_powerups_on_hand = 1;
            speed_boost_time = 3f;
            speed_boost_timer = speed_boost_time;
            starting_position = this.transform.position;
        }

        private void Update()
        {
            if (damage_invincibility)
            {
                if (UpdateTimer(ref damage_invincibility_timer, damageInvincibilityTime))
                    damage_invincibility = false;
            }

            HandleInput();

            // update animator
            if (animator != null)
            {
                animator.SetBool("isMoving", moving_horizontally);
                animator.SetBool("isDying", dying);
                animator.SetBool("isHurting", hurting);
                animator.SetBool("isDmgInvincible", damage_invincibility);
            }
        }

        private void HandleInput()
        {
            if (dying)
            {
                rigidbody_2d.velocity = new Vector2(0f, 0.1f);
                return;
            }

            if (Input.GetButton("Jump") && powerups.Count > 0)
            {
                speed_modifier = 3f;
                powerups.RemoveAt(0);
                powerUpUIObject.SetActive(false);
            }

            if (speed_modifier > 1f && UpdateTimer(ref speed_boost_timer, speed_boost_time))
                speed_modifier = 1f;

            // calculate weight modifier
            weight = Mathf.Clamp(gold_on_hand * 0.01f, 0, 0.5f);
            float weight_modifier = (1 - weight);

            float horizontal = Input.GetAxis("Horizontal") * moveSpeed;
            float vertical = Input.GetAxis("Vertical") * moveSpeed;

            moving_horizontally = !Mathf.Approximately(horizontal, 0f) || vertical < 0f;
            if (moving_horizontally)
            {
                vertical_collider.enabled = false;
                horizontal_collider.enabled = true;
                player_collider = horizontal_collider;
            }
            else
            {
                horizontal_collider.enabled = false;
                vertical_collider.enabled = true;
                player_collider = vertical_collider;
            }

            moving = !Mathf.Approximately(horizontal, 0f) || !Mathf.Approximately(vertical, 0f);
            if (moving)
            {
                // horizontal movement
                rigidbody_2d.velocity = new Vector2(weight_modifier * speed_modifier * horizontal * Time.deltaTime, rigidbody_2d.velocity.y);

                // upwards movement
                if (vertical > 0f && ((rigidbody_2d.velocity.y <= 1e-3 && UpdateTimer(ref upwards_timer, upwards_delay)) || rigidbody_2d.velocity.y <= 1e-3))
                {
                    rigidbody_2d.AddForce(new Vector2(0, weight_modifier * speed_modifier * upwardsForce * Time.deltaTime), ForceMode2D.Impulse);
                }

                if (horizontal != 0)
                    FaceDirection(horizontal > 0f);
            }
            
            // apply constant downwards force
            Vector2 smoothed_velocity = Vector2.Lerp(rigidbody_2d.velocity, new Vector2(0, vertical >= 0f ? weight_modifier * speed_modifier * sinkVelocity * Time.deltaTime : weight_modifier * speed_modifier * sinkVelocity * Time.deltaTime * 2), 0.025f);
            rigidbody_2d.velocity = smoothed_velocity;
        }

        private void FaceDirection(bool facing_right)
        {
            if (spriteRenderer != null)
                spriteRenderer.flipX = !facing_right;
            if (bubbles_sprite != null)
                bubbles_sprite.flipX = !facing_right;
        }

        public void OnHurtComplete()
        {
            hurting = false;
            if (health <= 0)
            {
                gold_on_hand = 0;
                goldText.text = gold_on_hand.ToString();
                dying = true;
                hurting = true;
                damage_invincibility = false;
                moving = false;
                lives--;
                lifeCounterText.text = Mathf.Max(lives, 0).ToString();
            }

        }

        public void OnDyingComplete()
        {
            if (lives < 0)
            {
                SceneLoader.Instance.LoadScene("GameOver", SceneLoader.TransitionType.FADE);
            }
            else
            {
                dying = false;
                damage_invincibility = true;
                moving = true;
                hurting = false;
                health = 2;
                this.transform.position = new Vector3(starting_position.x, starting_position.y, this.transform.position.z);
                healthUIObject.transform.GetChild(0).gameObject.SetActive(true);
                healthUIObject.transform.GetChild(1).gameObject.SetActive(true);
            }
        }

        private bool UpdateTimer(ref float timer, float resetTime)
        {
            if (timer > 0f)
                timer -= Time.deltaTime;
            else
            {
                timer = resetTime;
                return true;
            }

            return false;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Collectible"))
            {
                if (collision.gameObject.tag == "Gold")
                {
                    Gold gold = collision.gameObject.GetComponent<Gold>();
                    goldPickupSFX.Play();
                    gold_on_hand += (int)gold.Type;
                    goldText.text = gold_on_hand > 99 ? "100+" : gold_on_hand.ToString();
                    Destroy(collision.gameObject);
                }
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy") && !dying)
            {
                if (collision.gameObject.tag == "Shark" && !damage_invincibility && !dying)
                    collision.gameObject.GetComponent<Shark>().PlaySoundEffect();

                TakeDamage();
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Interactible"))
            {
                if (collision.gameObject.name == "vacuum")
                {
                    if (gold_on_hand > 0 && !dying)
                    {
                        collision.gameObject.GetComponent<Animator>().Play("suck", -1, 0f);
                        GameManager.Instance.Score += gold_on_hand;
                        scoreText.text = GameManager.Instance.Score.ToString("D4");
                        gold_on_hand = 0;
                        goldText.text = gold_on_hand > 99 ? "100+" : gold_on_hand.ToString();
                    }
                }
            }
            else if (collision.gameObject.tag == "PowerUp")
            {
                if (powerups.Count < max_powerups_on_hand)
                {
                    PowerUp powerup = collision.gameObject.GetComponent<PowerUp>();
                    if (powerup != null)
                    {
                        powerups.Add(powerup);
                        powerUpUIObject.SetActive(true);
                        Destroy(powerup.gameObject);
                    }
                }
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                TakeDamage();
            }
        }

        public void TakeDamage()
        {
            if (!damage_invincibility && !dying)
            {
                health--;
                healthUIObject.transform.GetChild(health).gameObject.SetActive(false);
                hurting = true;
                damage_invincibility = true;
            }
        }
    }
}
